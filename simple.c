#include <stdio.h>  
#include <stdlib.h>

// Min Max --------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------


int maximum(int *t, int n){
    int max = t[0];
    for ( int i = 1; i<n ; i++){
        if ( t[i] > max ) max = t[i];
    }
    return max;

}

void f(int a, int b, int *s, int *p) {
    *s = a + b;
    *p = a * b;
}
// *s et *p étant des pointeurs, ils vont pointer vers les valeurs qui leurs sont associés x et y 
// donc les operation effectuer a *s et *p ecriront sur la case mémoire associé a x et y.

void minmax(int *t, int n, int *pmin, int *pmax){
    int max = t[0], min = t[0];
    for ( int i = 1; i<n ; i++){
        if ( t[i] > max ) max = t[i];
        else if (t[i] < max) min = t[i];
    }
    *pmin = min;
    *pmax = max;
}

// Création Tableaux Dynamique ------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------


int* copie(int *tab, int n) { //On doit passer la taille du tableau en parametre car on ne peut pas retrouver la taille d'un tableau une fois creer
    int *tab2 = malloc(sizeof(int) * n );
    
    for (int i = 0; i < n; i++) {
        tab2[i] = tab[i];
    }
    return tab2;
}

int* unsurdeux(int *tab, int n){
    int *tab2 = malloc(sizeof(int) * n );

    for(int i = 0, j = 0; i < (n+1) ; i = i+2, j++ ){
        tab2[j] = tab[i];
    }
    return tab2;
}


// Matrice --------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------

struct Matrice {
    int nb_lignes;
    int nb_colonnes;
    int **valeurs;
};

struct Matrice matrice(int nbl, int nbc, int *valeurs){
    int **mat = malloc(nbl * sizeof(int*));
    for (int i = 0; i < nbl; i++){
        int *tab = malloc(nbc * sizeof(int));
        for (int j = 0; j < nbc ; j++){
            tab[j] = valeurs[j + nbc * i];
        }
        mat[i] = tab;

        
    }
    struct Matrice res;
    res.nb_colonnes = nbc;
    res.nb_lignes = nbl;
    res.valeurs = mat;
    
    return res;
}

void efface(struct Matrice mat){
    for (int i = 0; i < mat.nb_lignes; i++){
        free(mat.valeurs[i]);
    }
    free(mat.valeurs);
}

struct Matrice multiplie(struct Matrice mat1, struct Matrice mat2) {
    struct Matrice res;
    int **mat = malloc(mat1.nb_lignes * sizeof(int*));
    for (int i = 0; i < mat1.nb_lignes; i ++) {
        int *ligne = malloc(mat2.nb_colonnes * sizeof(int));
        for (int j = 0; j < mat2.nb_colonnes; j ++) {
            int s = 0;
            for (int k = 0; k < mat2.nb_lignes; k ++) 
                s = s + (mat2.valeurs[k][j] * mat1.valeurs[i][k]);
            ligne[j] = s;
        }
        mat[i] = ligne;
    }

    res.nb_colonnes = mat2.nb_colonnes;
    res.nb_lignes = mat1.nb_lignes;
    res.valeurs = mat;

    return res;
}




int main() {  
    
    /*int tab[4] = {23464347,2,3,-4};
    int min,max;
    int* tab2 = copie(tab, 4);

    for(int i = 0; i<4;i++){
       printf("valeur %i : %i \n" , i ,  tab2[i]);

    }*/

    int tab[9] = {
        9,3,4,
        1,6,8,
        2,3,4
    };

    int tab2[9] = {
        2,3,4,
        13,4,5,
        4,3,4
    };

    struct Matrice mat = matrice(3,3,tab);
    struct Matrice mat2 = matrice(3,3,tab2);

    //printf("%i , %i \n", mat.nb_colonnes , mat.nb_lignes);
    //printf("%i , %i", mat.valeurs[0][0] , mat.valeurs[0][1]);

    for (int i = 0; i < mat.nb_lignes; i ++) {
        int *tab = mat.valeurs[i];
        for (int j = 0; j < mat.nb_colonnes; j ++) {
            printf("%i, ", mat.valeurs[i][j]);
        }
        printf("\n");
    }
    printf("\n");

    for (int i = 0; i < mat2.nb_lignes; i ++) {
        int *tab2 = mat2.valeurs[i];
        for (int j = 0; j < mat2.nb_colonnes; j ++) {
            printf("%i, ", mat2.valeurs[i][j]);
        }
        printf("\n");
    }


    struct Matrice mat3 = multiplie(mat,mat2);

    printf("\n");

    for (int i = 0; i < mat3.nb_lignes; i ++) {
        int *tab3 = mat3.valeurs[i];
        for (int j = 0; j < mat3.nb_colonnes; j ++) {
            printf("%i, ", mat3.valeurs[i][j]);
        }
        printf("\n");
    }

}  


